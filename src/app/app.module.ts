import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainBlockComponent } from './src/app/layout/main-block/main-block.component';
import { TitleComponent } from './src/app/layout/title/title.component';
import { EntryComponent } from './src/app/layout/entry/entry.component';
import { SubentryComponent } from './src/app/layout/entry/subentry/subentry.component';

import { MobxAngularModule } from 'mobx-angular';
import { LedComponent } from './src/app/layout/entry/led/led.component';
import { AddTODOComponent } from './src/app/layout/add-todo/add-todo.component';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MainBlockComponent,
    TitleComponent,
    EntryComponent,
    SubentryComponent,
    LedComponent,
    AddTODOComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MobxAngularModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
