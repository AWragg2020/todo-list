import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTODOComponent } from './add-todo.component';

describe('AddTODOComponent', () => {
  let component: AddTODOComponent;
  let fixture: ComponentFixture<AddTODOComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTODOComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTODOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
