import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ToDoStore } from '../../store/to-do-store.service';


@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddTODOComponent {

  placeholder = 'Add ToDo by writing here, then hit ⏎';
  value = '';

  constructor(public store: ToDoStore) { }

  onEnter() {
    if (this.value && this.value.trim().length !== 0) {
      this.store.addToDo(this.value);
    }
  }

}
