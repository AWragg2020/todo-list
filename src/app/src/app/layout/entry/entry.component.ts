import { Component, ChangeDetectionStrategy, Input, OnInit } from '@angular/core';
import { ToDoStore } from '../../store/to-do-store.service';
import { delay } from 'q';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntryComponent implements OnInit {

  @Input() description: string;
  @Input() index: number;
  status: number;

  deleted = false;
  done: boolean;
  showActionButtons = false;

  ngOnInit() {
    this.status = this.store.status[this.index];
    this.done = this.isDone();
  }

  constructor(public store: ToDoStore) {
  }

  async onDelete() {
    this.deleted = true;
    await delay(600);
    this.store.removeToDo(this.index);
  }

  onEdit() {
    this.store.updateDescription(this.index, this.description);
  }

  onHoverIn() {
    this.showActionButtons = true;
  }

  onHoverOut() {
    this.showActionButtons = false;
  }

  updateStatus(event: number) {
    this.status = event;
    this.done = this.isDone();
    this.store.updateState(this.index, this.status);
  }

  private isDone() {
    return this.status === 2;
  }

}
