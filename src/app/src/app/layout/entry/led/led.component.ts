import { Component, OnInit, ChangeDetectionStrategy, Output, Input } from '@angular/core';
import { ToDoStore } from '../../../store/to-do-store.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-led',
  templateUrl: './led.component.html',
  styleUrls: ['./led.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LedComponent implements OnInit {

  @Output() ledClick: EventEmitter<number> = new EventEmitter<number>();
  @Input() index: number;

  status: number;

  constructor(public store: ToDoStore) { }

  ngOnInit() {
    this.status = this.store.status[this.index];
  }

  changeStatus() {
    this.status = (this.status + 1) % 3;
    this.ledClick.emit(this.status);
  }

}
