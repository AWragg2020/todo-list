import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ToDoStore, ToDo } from '../../store/to-do-store.service';

@Component({
  selector: 'app-main-block',
  templateUrl: './main-block.component.html',
  styleUrls: ['./main-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainBlockComponent implements OnInit {

  todos: ToDo[] = this.store.todos;

  constructor(public store: ToDoStore) { }

  ngOnInit() {
  }

}
