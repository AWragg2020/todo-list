import { TestBed } from '@angular/core/testing';

import { ToDoStore } from './to-do-store.service';

describe('ToDoStore', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToDoStore = TestBed.get(ToDoStore);
    expect(service).toBeTruthy();
  });
});
