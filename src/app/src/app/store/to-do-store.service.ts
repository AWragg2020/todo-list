import { Injectable } from '@angular/core';
import { observable, action, computed } from 'mobx';

export class ToDo {
  description: string;
  status = 0;

  constructor(description: string, status: number) {
    this.description = description;
    this.status = status;
  }
}

const defaultToDos = [
  new ToDo('This is a regular ToDo. You can edit and remove it.', 0),
  new ToDo('This ToDo is in progress.', 1),
  new ToDo('This ToDo is done!', 2)
];

@Injectable({
  providedIn: 'root'
})
export class ToDoStore {
  @observable todos: ToDo[] = [];

  constructor() {
    const localStorageTodos = localStorage.getItem('todos');
    if (!localStorageTodos || localStorageTodos === null) {
      this.todos = defaultToDos;
      localStorage.setItem('todos', JSON.stringify(this.todos));
    } else {
      this.todos = JSON.parse(localStorage.getItem('todos'));
    }
  }

  @action addToDo(description: string) {
    this.todos.push(new ToDo(description, 0));
    this.updateLocalStorageToDos();
  }
  @action removeToDo(index: number) {
    this.todos.splice(index, 1);
    this.updateLocalStorageToDos();
  }
  @action updateDescription(index: number, description: string) {
    this.todos[index].description = description;
    this.updateLocalStorageToDos();
  }
  @action updateState(index: number, status: number) {
    this.todos[index].status = status;
    this.updateLocalStorageToDos();
  }

  @computed get status() {
    return this.todos.map(
      todo => {
        return todo.status;
      }
    );
  }

  private updateLocalStorageToDos() {
    localStorage.setItem('todos', JSON.stringify(this.todos));
  }
}
